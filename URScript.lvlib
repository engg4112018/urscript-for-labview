﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91M&lt;/W-,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@P_W`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"&lt;Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"O[^NF!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="SubVI&apos;s" Type="Folder">
		<Item Name="commands" Type="Folder"/>
		<Item Name="Conditional" Type="Folder">
			<Item Name="Elif.vi" Type="VI" URL="../URScript/conidtionals/Elif.vi"/>
			<Item Name="Else.vi" Type="VI" URL="../URScript/conidtionals/Else.vi"/>
			<Item Name="If.vi" Type="VI" URL="../URScript/conidtionals/If.vi"/>
		</Item>
		<Item Name="expressions" Type="Folder">
			<Item Name="Add.vi" Type="VI" URL="../URScript/expressions/Add.vi"/>
			<Item Name="Assign.vi" Type="VI" URL="../URScript/expressions/Assign.vi"/>
			<Item Name="DegToRad.vi" Type="VI" URL="../URScript/utils/DegToRad.vi"/>
			<Item Name="GravityVector.vi" Type="VI" URL="../URScript/expressions/GravityVector.vi"/>
			<Item Name="JointPos.vi" Type="VI" URL="../URScript/expressions/JointPos.vi"/>
			<Item Name="JointSpeed.vi" Type="VI" URL="../URScript/expressions/JointSpeed.vi"/>
			<Item Name="Pose.vi" Type="VI" URL="../URScript/expressions/Pose.vi"/>
			<Item Name="SpacialVector.vi" Type="VI" URL="../URScript/expressions/SpacialVector.vi"/>
			<Item Name="URArray.vi" Type="VI" URL="../URScript/expressions/URArray.vi"/>
			<Item Name="URString.vi" Type="VI" URL="../URScript/expressions/URString.vi"/>
		</Item>
		<Item Name="functions" Type="Folder">
			<Item Name="Def.vi" Type="VI" URL="../URScript/expressions/Def.vi"/>
			<Item Name="End.vi" Type="VI" URL="../URScript/expressions/End.vi"/>
			<Item Name="While.vi" Type="VI" URL="../While.vi"/>
		</Item>
		<Item Name="interfaces" Type="Folder">
			<Item Name="functions" Type="Folder">
				<Item Name="GetAnalogIn.vi" Type="VI" URL="../URScript/interfaces/GetAnalogIn.vi"/>
				<Item Name="GetAnalogOut.vi" Type="VI" URL="../URScript/interfaces/GetAnalogOut.vi"/>
				<Item Name="GetConfigurableDigitalIn.vi" Type="VI" URL="../URScript/interfaces/GetConfigurableDigitalIn.vi"/>
				<Item Name="GetConfigurableDigitalOut.vi" Type="VI" URL="../URScript/interfaces/GetConfigurableDigitalOut.vi"/>
				<Item Name="GetDigitalIn.vi" Type="VI" URL="../URScript/interfaces/GetDigitalIn.vi"/>
				<Item Name="GetDigitalOut.vi" Type="VI" URL="../URScript/interfaces/GetDigitalOut.vi"/>
				<Item Name="GetEuromapInput.vi" Type="VI" URL="../URScript/interfaces/GetEuromapInput.vi"/>
				<Item Name="GetEuromapOutput.vi" Type="VI" URL="../URScript/interfaces/GetEuromapOutput.vi"/>
				<Item Name="GetFlag.vi" Type="VI" URL="../URScript/interfaces/GetFlag.vi"/>
				<Item Name="GetStandardAnalogIn.vi" Type="VI" URL="../URScript/interfaces/GetStandardAnalogIn.vi"/>
				<Item Name="GetStandardAnalogueOut.vi" Type="VI" URL="../URScript/interfaces/GetStandardAnalogueOut.vi"/>
				<Item Name="GetStandardDigitalIn.vi" Type="VI" URL="../URScript/interfaces/GetStandardDigitalIn.vi"/>
				<Item Name="GetStandardDigitalOut.vi" Type="VI" URL="../URScript/interfaces/GetStandardDigitalOut.vi"/>
				<Item Name="GetToolAnalogIn.vi" Type="VI" URL="../URScript/interfaces/GetToolAnalogIn.vi"/>
				<Item Name="GetToolAnalogOut.vi" Type="VI" URL="../URScript/interfaces/GetToolAnalogOut.vi"/>
				<Item Name="GetToolDigitalIn.vi" Type="VI" URL="../URScript/interfaces/GetToolDigitalIn.vi"/>
				<Item Name="GetToolDigitalOut.vi" Type="VI" URL="../URScript/interfaces/GetToolDigitalOut.vi"/>
				<Item Name="ModbusAddSignal.vi" Type="VI" URL="../URScript/interfaces/ModbusAddSignal.vi"/>
				<Item Name="ModbusDeleteSignalvi.vi" Type="VI" URL="../URScript/interfaces/ModbusDeleteSignalvi.vi"/>
				<Item Name="ModbusGetSignalStatus.vi" Type="VI" URL="../URScript/interfaces/ModbusGetSignalStatus.vi"/>
				<Item Name="ModbusSendCustomCommand.vi" Type="VI" URL="../URScript/interfaces/ModbusSendCustomCommand.vi"/>
				<Item Name="ModbusSetOutputRegister.vi" Type="VI" URL="../URScript/interfaces/ModbusSetOutputRegister.vi"/>
				<Item Name="ModbusSetRunstateDependentChoice.vi" Type="VI" URL="../URScript/interfaces/ModbusSetRunstateDependentChoice.vi"/>
				<Item Name="ModbusSetSignalUpdateFrequency.vi" Type="VI" URL="../URScript/interfaces/ModbusSetSignalUpdateFrequency.vi"/>
				<Item Name="modebusSetOutputSignal.vi" Type="VI" URL="../URScript/interfaces/modebusSetOutputSignal.vi"/>
				<Item Name="ReadInputBooleanRegister.vi" Type="VI" URL="../URScript/interfaces/ReadInputBooleanRegister.vi"/>
				<Item Name="ReadInputFloatRegister.vi" Type="VI" URL="../URScript/interfaces/ReadInputFloatRegister.vi"/>
				<Item Name="ReadInputIntegerRegister.vi" Type="VI" URL="../URScript/interfaces/ReadInputIntegerRegister.vi"/>
				<Item Name="ReadOuputFloatRegister.vi" Type="VI" URL="../URScript/interfaces/ReadOuputFloatRegister.vi"/>
				<Item Name="ReadOutputBooleamRegister.vi" Type="VI" URL="../URScript/interfaces/ReadOutputBooleamRegister.vi"/>
				<Item Name="ReadOutputIntegerRegister.vi" Type="VI" URL="../URScript/interfaces/ReadOutputIntegerRegister.vi"/>
				<Item Name="ReadPortBit.vi" Type="VI" URL="../URScript/interfaces/ReadPortBit.vi"/>
				<Item Name="ReadPortRegister.vi" Type="VI" URL="../URScript/interfaces/ReadPortRegister.vi"/>
				<Item Name="RpcFactory.vi" Type="VI" URL="../URScript/interfaces/RpcFactory.vi"/>
				<Item Name="RtdeSetWatchdog.vi" Type="VI" URL="../URScript/interfaces/RtdeSetWatchdog.vi"/>
				<Item Name="SetAnalogInputrange.vi" Type="VI" URL="../URScript/interfaces/SetAnalogInputrange.vi"/>
				<Item Name="SetAnalogOut.vi" Type="VI" URL="../URScript/interfaces/SetAnalogOut.vi"/>
				<Item Name="SetAnalogOutputdomain.vi" Type="VI" URL="../URScript/interfaces/SetAnalogOutputdomain.vi"/>
				<Item Name="SetConfigurableDigitalOut.vi" Type="VI" URL="../URScript/interfaces/SetConfigurableDigitalOut.vi"/>
				<Item Name="SetDigialOut.vi" Type="VI" URL="../URScript/interfaces/SetDigialOut.vi"/>
				<Item Name="SetEuromapOutput.vi" Type="VI" URL="../URScript/interfaces/SetEuromapOutput.vi"/>
				<Item Name="SetEuromapRunstateDepenedentChoice.vi" Type="VI" URL="../URScript/interfaces/SetEuromapRunstateDepenedentChoice.vi"/>
				<Item Name="SetFlag.vi" Type="VI" URL="../URScript/interfaces/SetFlag.vi"/>
				<Item Name="SetRunstateConfigurableDigitalOutputToValue.vi" Type="VI" URL="../URScript/interfaces/SetRunstateConfigurableDigitalOutputToValue.vi"/>
				<Item Name="SetRunstateGpBooleanOutputToValue.vi" Type="VI" URL="../URScript/interfaces/SetRunstateGpBooleanOutputToValue.vi"/>
				<Item Name="SetRunstateStandardAnalogOutputToValue.vi" Type="VI" URL="../URScript/interfaces/SetRunstateStandardAnalogOutputToValue.vi"/>
				<Item Name="SetRunstateStandardDigitalOutputToValue.vi" Type="VI" URL="../URScript/interfaces/SetRunstateStandardDigitalOutputToValue.vi"/>
				<Item Name="SetRunstateToolDigitalOutputToValue.vi" Type="VI" URL="../URScript/interfaces/SetRunstateToolDigitalOutputToValue.vi"/>
				<Item Name="SetStandardAnalogInputDomain.vi" Type="VI" URL="../URScript/interfaces/SetStandardAnalogInputDomain.vi"/>
				<Item Name="SetStandardAnalogOut.vi" Type="VI" URL="../URScript/interfaces/SetStandardAnalogOut.vi"/>
				<Item Name="SetStandardDigitalOut.vi" Type="VI" URL="../URScript/interfaces/SetStandardDigitalOut.vi"/>
				<Item Name="SetToolAnalogInputDomain.vi" Type="VI" URL="../URScript/interfaces/SetToolAnalogInputDomain.vi"/>
				<Item Name="SetToolCommication.vi" Type="VI" URL="../URScript/interfaces/SetToolCommication.vi"/>
				<Item Name="SetToolDigitalOut.vi" Type="VI" URL="../URScript/interfaces/SetToolDigitalOut.vi"/>
				<Item Name="SetToolVoltage.vi" Type="VI" URL="../URScript/interfaces/SetToolVoltage.vi"/>
				<Item Name="SocketClose.vi" Type="VI" URL="../URScript/interfaces/SocketClose.vi"/>
				<Item Name="SocketGetVar.vi" Type="VI" URL="../URScript/interfaces/SocketGetVar.vi"/>
				<Item Name="SocketOpen.vi" Type="VI" URL="../URScript/interfaces/SocketOpen.vi"/>
				<Item Name="SocketReadAsciiFloat.vi" Type="VI" URL="../URScript/interfaces/SocketReadAsciiFloat.vi"/>
				<Item Name="SocketReadBinaryInteger.vi" Type="VI" URL="../URScript/interfaces/SocketReadBinaryInteger.vi"/>
				<Item Name="SocketReadByteList.vi" Type="VI" URL="../URScript/interfaces/SocketReadByteList.vi"/>
				<Item Name="SocketReadLine.vi" Type="VI" URL="../URScript/interfaces/SocketReadLine.vi"/>
				<Item Name="SocketReadString.vi" Type="VI" URL="../URScript/interfaces/SocketReadString.vi"/>
				<Item Name="SocketSendByte.vi" Type="VI" URL="../URScript/interfaces/SocketSendByte.vi"/>
				<Item Name="SocketSendInt.vi" Type="VI" URL="../URScript/interfaces/SocketSendInt.vi"/>
				<Item Name="SocketSendLine.vi" Type="VI" URL="../URScript/interfaces/SocketSendLine.vi"/>
				<Item Name="SocketSendString.vi" Type="VI" URL="../URScript/interfaces/SocketSendString.vi"/>
				<Item Name="SocketSetVar.vi" Type="VI" URL="../URScript/interfaces/SocketSetVar.vi"/>
				<Item Name="WriteOutputBooleanRegister.vi" Type="VI" URL="../URScript/interfaces/WriteOutputBooleanRegister.vi"/>
				<Item Name="WriteOutputFloatRegister.vi" Type="VI" URL="../URScript/interfaces/WriteOutputFloatRegister.vi"/>
				<Item Name="WriteOutputIntegerRegister.vi" Type="VI" URL="../URScript/interfaces/WriteOutputIntegerRegister.vi"/>
				<Item Name="WritePortBit.vi" Type="VI" URL="../URScript/interfaces/WritePortBit.vi"/>
				<Item Name="WritePortRegister.vi" Type="VI" URL="../URScript/interfaces/WritePortRegister.vi"/>
				<Item Name="ZeroFtsensor.vi" Type="VI" URL="../URScript/interfaces/ZeroFtsensor.vi"/>
			</Item>
			<Item Name="variables" Type="Folder"/>
		</Item>
		<Item Name="internals" Type="Folder">
			<Item Name="functions" Type="Folder">
				<Item Name="Force.vi" Type="VI" URL="../URScript/internals/Force.vi"/>
				<Item Name="GetActualJoingSpeeds.vi" Type="VI" URL="../URScript/internals/GetActualJoingSpeeds.vi"/>
				<Item Name="GetActualJointPositions.vi" Type="VI" URL="../URScript/internals/GetActualJointPositions.vi"/>
				<Item Name="GetActualTcpPose.vi" Type="VI" URL="../URScript/internals/GetActualTcpPose.vi"/>
				<Item Name="GetActualTcpSpeed.vi" Type="VI" URL="../URScript/internals/GetActualTcpSpeed.vi"/>
				<Item Name="GetControllerTemp.vi" Type="VI" URL="../URScript/internals/GetControllerTemp.vi"/>
				<Item Name="GetInverseKin.vi" Type="VI" URL="../URScript/internals/GetInverseKin.vi"/>
				<Item Name="GetJointTemp.vi" Type="VI" URL="../URScript/internals/GetJointTemp.vi"/>
				<Item Name="GetJointTorques.vi" Type="VI" URL="../URScript/internals/GetJointTorques.vi"/>
				<Item Name="GetTargetJointPositions.vi" Type="VI" URL="../URScript/internals/GetTargetJointPositions.vi"/>
				<Item Name="GetTargetJointsSpeeds.vi" Type="VI" URL="../URScript/internals/GetTargetJointsSpeeds.vi"/>
				<Item Name="GetTargetTcpPose.vi" Type="VI" URL="../URScript/internals/GetTargetTcpPose.vi"/>
				<Item Name="GetTargetTcpSpeed.vi" Type="VI" URL="../URScript/internals/GetTargetTcpSpeed.vi"/>
				<Item Name="GetTcpForce.vi" Type="VI" URL="../URScript/internals/GetTcpForce.vi"/>
				<Item Name="Popup.vi" Type="VI" URL="../URScript/internals/Popup.vi"/>
				<Item Name="SetGravity.vi" Type="VI" URL="../URScript/internals/SetGravity.vi"/>
				<Item Name="SetPayload.vi" Type="VI" URL="../URScript/internals/SetPayload.vi"/>
				<Item Name="SetTcp.vi" Type="VI" URL="../URScript/internals/SetTcp.vi"/>
				<Item Name="Sleep.vi" Type="VI" URL="../URScript/internals/Sleep.vi"/>
				<Item Name="Sync.vi" Type="VI" URL="../URScript/internals/Sync.vi"/>
				<Item Name="TextMsg.vi" Type="VI" URL="../URScript/internals/TextMsg.vi"/>
			</Item>
			<Item Name="variables" Type="Folder"/>
		</Item>
		<Item Name="motion" Type="Folder">
			<Item Name="functions" Type="Folder">
				<Item Name="ConveyorPulseDecode.vi" Type="VI" URL="../URScript/motion/ConveyorPulseDecode.vi"/>
				<Item Name="EndForceMode.vi" Type="VI" URL="../URScript/motion/EndForceMode.vi"/>
				<Item Name="EndFreedriveMode.vi" Type="VI" URL="../URScript/motion/EndFreedriveMode.vi"/>
				<Item Name="EndTeachMode.vi" Type="VI" URL="../URScript/motion/EndTeachMode.vi"/>
				<Item Name="ForceMode.vi" Type="VI" URL="../URScript/motion/ForceMode.vi"/>
				<Item Name="FreedriveMode.vi" Type="VI" URL="../URScript/motion/FreedriveMode.vi"/>
				<Item Name="GetConveyorTickCount.vi" Type="VI" URL="../URScript/motion/GetConveyorTickCount.vi"/>
				<Item Name="Movec.vi" Type="VI" URL="../URScript/motion/Movec.vi"/>
				<Item Name="Movej.vi" Type="VI" URL="../URScript/motion/Movej.vi"/>
				<Item Name="movel.vi" Type="VI" URL="../URScript/motion/movel.vi"/>
				<Item Name="Movep.vi" Type="VI" URL="../URScript/motion/Movep.vi"/>
				<Item Name="Servoc.vi" Type="VI" URL="../URScript/motion/Servoc.vi"/>
				<Item Name="SetConveyorTickCount.vi" Type="VI" URL="../URScript/motion/SetConveyorTickCount.vi"/>
				<Item Name="setPos.vi" Type="VI" URL="../URScript/motion/setPos.vi"/>
				<Item Name="speedj.vi" Type="VI" URL="../URScript/motion/speedj.vi"/>
				<Item Name="Speedl.vi" Type="VI" URL="../URScript/motion/Speedl.vi"/>
				<Item Name="StopConveyorTracking.vi" Type="VI" URL="../URScript/motion/StopConveyorTracking.vi"/>
				<Item Name="Stopj.vi" Type="VI" URL="../URScript/motion/Stopj.vi"/>
				<Item Name="Stopl.vi" Type="VI" URL="../URScript/motion/Stopl.vi"/>
				<Item Name="TeachMode.vi" Type="VI" URL="../URScript/motion/TeachMode.vi"/>
				<Item Name="TrackConveyorCircular.vi" Type="VI" URL="../URScript/motion/TrackConveyorCircular.vi"/>
				<Item Name="TrackConveyorLinear.vi" Type="VI" URL="../URScript/motion/TrackConveyorLinear.vi"/>
			</Item>
			<Item Name="variables" Type="Folder"/>
		</Item>
		<Item Name="RTDE" Type="Folder">
			<Item Name="extract" Type="Folder">
				<Item Name="Extract_BOOL.vi" Type="VI" URL="../URScript/RTDE/Extract_BOOL.vi"/>
				<Item Name="Extract_Control.vi" Type="VI" URL="../URScript/RTDE/Extract_Control.vi"/>
				<Item Name="Extract_DOUBLE.vi" Type="VI" URL="../URScript/RTDE/Extract_DOUBLE.vi"/>
				<Item Name="Extract_INT32.vi" Type="VI" URL="../URScript/RTDE/Extract_INT32.vi"/>
				<Item Name="Extract_UNIT8.vi" Type="VI" URL="../URScript/RTDE/Extract_UNIT8.vi"/>
				<Item Name="Extract_UNIT32.vi" Type="VI" URL="../URScript/RTDE/Extract_UNIT32.vi"/>
				<Item Name="Extract_UNIT64.vi" Type="VI" URL="../URScript/RTDE/Extract_UNIT64.vi"/>
				<Item Name="Extract_VECTOR3D.vi" Type="VI" URL="../URScript/RTDE/Extract_VECTOR3D.vi"/>
				<Item Name="Extract_VECTOR6D.vi" Type="VI" URL="../URScript/RTDE/Extract_VECTOR6D.vi"/>
				<Item Name="Extract_VECTOR6INT32.vi" Type="VI" URL="../URScript/RTDE/Extract_VECTOR6INT32.vi"/>
				<Item Name="Extract_VECTOR6UINT32.vi" Type="VI" URL="../URScript/RTDE/Extract_VECTOR6UINT32.vi"/>
			</Item>
			<Item Name="get" Type="Folder">
				<Item Name="Get_BOOL.vi" Type="VI" URL="../URScript/RTDE/Get_BOOL.vi"/>
				<Item Name="Get_Control.vi" Type="VI" URL="../URScript/RTDE/Get_Control.vi"/>
				<Item Name="Get_DOUBLE.vi" Type="VI" URL="../URScript/RTDE/Get_DOUBLE.vi"/>
				<Item Name="Get_INT32.vi" Type="VI" URL="../URScript/RTDE/Get_INT32.vi"/>
				<Item Name="Get_UINT8.vi" Type="VI" URL="../URScript/RTDE/Get_UINT8.vi"/>
				<Item Name="Get_UINT32.vi" Type="VI" URL="../URScript/RTDE/Get_UINT32.vi"/>
				<Item Name="Get_UINT64.vi" Type="VI" URL="../URScript/RTDE/Get_UINT64.vi"/>
				<Item Name="Get_VECTOR3D.vi" Type="VI" URL="../URScript/RTDE/Get_VECTOR3D.vi"/>
				<Item Name="Get_VECTOR6D.vi" Type="VI" URL="../URScript/RTDE/Get_VECTOR6D.vi"/>
				<Item Name="Get_VECTOR6INT32.vi" Type="VI" URL="../URScript/RTDE/Get_VECTOR6INT32.vi"/>
				<Item Name="Get_VECTOR6UINT32.vi" Type="VI" URL="../URScript/RTDE/Get_VECTOR6UINT32.vi"/>
			</Item>
			<Item Name="Begin.vi" Type="VI" URL="../URScript/RTDE/Begin.vi"/>
			<Item Name="DetermineSize.vi" Type="VI" URL="../URScript/RTDE/DetermineSize.vi"/>
			<Item Name="EstablishTypes.vi" Type="VI" URL="../URScript/RTDE/EstablishTypes.vi"/>
			<Item Name="ExtractData.vi" Type="VI" URL="../URScript/RTDE/ExtractData.vi"/>
			<Item Name="Initialise.vi" Type="VI" URL="../URScript/RTDE/Initialise.vi"/>
			<Item Name="Listen.vi" Type="VI" URL="../URScript/RTDE/Listen.vi"/>
			<Item Name="RTDE.vi" Type="VI" URL="../URScript/RTDE/RTDE.vi"/>
			<Item Name="Write.vi" Type="VI" URL="../URScript/RTDE/Write.vi"/>
		</Item>
		<Item Name="TCP" Type="Folder">
			<Item Name="TCP_UR.vi" Type="VI" URL="../URScript/TCP/TCP_UR.vi"/>
		</Item>
		<Item Name="urmath" Type="Folder">
			<Item Name="functions" Type="Folder">
				<Item Name="acos.vi" Type="VI" URL="../URScript/urmath/acos.vi"/>
				<Item Name="asin.vi" Type="VI" URL="../URScript/urmath/asin.vi"/>
				<Item Name="atan.vi" Type="VI" URL="../URScript/urmath/atan.vi"/>
				<Item Name="atan2.vi" Type="VI" URL="../URScript/urmath/atan2.vi"/>
				<Item Name="BinaryListToInteger.vi" Type="VI" URL="../URScript/urmath/BinaryListToInteger.vi"/>
				<Item Name="ceil.vi" Type="VI" URL="../URScript/urmath/ceil.vi"/>
				<Item Name="cos.vi" Type="VI" URL="../URScript/urmath/cos.vi"/>
				<Item Name="d2r.vi" Type="VI" URL="../URScript/urmath/d2r.vi"/>
				<Item Name="floor.vi" Type="VI" URL="../URScript/urmath/floor.vi"/>
				<Item Name="GetListLength.vi" Type="VI" URL="../URScript/urmath/GetListLength.vi"/>
				<Item Name="IntegerToBinaryList.vi" Type="VI" URL="../URScript/urmath/IntegerToBinaryList.vi"/>
				<Item Name="InterpolatePose.vi" Type="VI" URL="../URScript/urmath/InterpolatePose.vi"/>
				<Item Name="Length.vi" Type="VI" URL="../URScript/urmath/Length.vi"/>
				<Item Name="log.vi" Type="VI" URL="../URScript/urmath/log.vi"/>
				<Item Name="Norm.vi" Type="VI" URL="../URScript/urmath/Norm.vi"/>
				<Item Name="PointDist.vi" Type="VI" URL="../URScript/urmath/PointDist.vi"/>
				<Item Name="PoseAdd.vi" Type="VI" URL="../URScript/urmath/PoseAdd.vi"/>
				<Item Name="PoseDist.vi" Type="VI" URL="../URScript/urmath/PoseDist.vi"/>
				<Item Name="PoseInv.vi" Type="VI" URL="../URScript/urmath/PoseInv.vi"/>
				<Item Name="PoseSub.vi" Type="VI" URL="../URScript/urmath/PoseSub.vi"/>
				<Item Name="PoseTrans.vi" Type="VI" URL="../URScript/urmath/PoseTrans.vi"/>
				<Item Name="pow.vi" Type="VI" URL="../URScript/urmath/pow.vi"/>
				<Item Name="r2d.vi" Type="VI" URL="../URScript/urmath/r2d.vi"/>
				<Item Name="Random.vi" Type="VI" URL="../URScript/urmath/Random.vi"/>
				<Item Name="sin.vi" Type="VI" URL="../URScript/urmath/sin.vi"/>
				<Item Name="sqrt.vi" Type="VI" URL="../URScript/urmath/sqrt.vi"/>
				<Item Name="tan.vi" Type="VI" URL="../URScript/urmath/tan.vi"/>
			</Item>
			<Item Name="variables" Type="Folder"/>
		</Item>
		<Item Name="utils" Type="Folder">
			<Item Name="BooleanToString.vi" Type="VI" URL="../URScript/utils/BooleanToString.vi"/>
			<Item Name="ConstructFunctionCall.vi" Type="VI" URL="../URScript/utils/ConstructFunctionCall.vi"/>
			<Item Name="Cross.vi" Type="VI" URL="../URScript/utils/Cross.vi"/>
			<Item Name="DecrementIndent.vi" Type="VI" URL="../URScript/utils/DecrementIndent.vi"/>
			<Item Name="Dot.vi" Type="VI" URL="../URScript/utils/Dot.vi"/>
			<Item Name="Format.vi" Type="VI" URL="../URScript/utils/Format.vi"/>
			<Item Name="FunctionCall.vi" Type="VI" URL="../URScript/utils/FunctionCall.vi"/>
			<Item Name="IncrementIndent.vi" Type="VI" URL="../URScript/utils/IncrementIndent.vi"/>
			<Item Name="Indent.vi" Type="VI" URL="../URScript/utils/Indent.vi"/>
			<Item Name="LookAt.vi" Type="VI" URL="../URScript/utils/LookAt.vi"/>
			<Item Name="LookAt2.vi" Type="VI" URL="../URScript/utils/LookAt2.vi"/>
			<Item Name="MetricListener.vi" Type="VI" URL="../URScript/utils/MetricListener.vi"/>
			<Item Name="NewLine.vi" Type="VI" URL="../URScript/utils/NewLine.vi"/>
			<Item Name="Normalize.vi" Type="VI" URL="../URScript/utils/Normalize.vi"/>
			<Item Name="ParseMetrics.vi" Type="VI" URL="../URScript/utils/ParseMetrics.vi"/>
			<Item Name="Posify.vi" Type="VI" URL="../URScript/utils/Posify.vi"/>
			<Item Name="Subtract.vi" Type="VI" URL="../URScript/utils/Subtract.vi"/>
		</Item>
	</Item>
	<Item Name="Types" Type="Folder">
		<Item Name="Command.ctl" Type="VI" URL="../URScript/expressions/Command.ctl"/>
		<Item Name="Control 1.ctl" Type="VI" URL="../Control 1.ctl"/>
		<Item Name="CurrentCode.ctl" Type="VI" URL="../URScript/expressions/CurrentCode.ctl"/>
		<Item Name="CurrentPose.ctl" Type="VI" URL="../URScript/utils/CurrentPose.ctl"/>
		<Item Name="Expression.ctl" Type="VI" URL="../URScript/expressions/Expression.ctl"/>
		<Item Name="FunctionCall.ctl" Type="VI" URL="../URScript/expressions/FunctionCall.ctl"/>
		<Item Name="GravityVector.ctl" Type="VI" URL="../URScript/expressions/GravityVector.ctl"/>
		<Item Name="JointPos.ctl" Type="VI" URL="../URScript/expressions/JointPos.ctl"/>
		<Item Name="PackageName.ctl" Type="VI" URL="../URScript/RTDE/PackageName.ctl"/>
		<Item Name="Parameters.ctl" Type="VI" URL="../URScript/expressions/Parameters.ctl"/>
		<Item Name="Pose.ctl" Type="VI" URL="../URScript/expressions/Pose.ctl"/>
		<Item Name="Radian.ctl" Type="VI" URL="../URScript/expressions/Radian.ctl"/>
		<Item Name="UINT8.ctl" Type="VI" URL="../URScript/RTDE/UINT8.ctl"/>
		<Item Name="UINT32.ctl" Type="VI" URL="../URScript/RTDE/UINT32.ctl"/>
		<Item Name="UINT64.ctl" Type="VI" URL="../URScript/RTDE/UINT64.ctl"/>
		<Item Name="URArray.ctl" Type="VI" URL="../URScript/expressions/URArray.ctl"/>
		<Item Name="URString.ctl" Type="VI" URL="../URScript/expressions/URString.ctl"/>
		<Item Name="Vector3.ctl" Type="VI" URL="../URScript/utils/Vector3.ctl"/>
		<Item Name="Vector3D.ctl" Type="VI" URL="../URScript/RTDE/Vector3D.ctl"/>
		<Item Name="Vector6D.ctl" Type="VI" URL="../URScript/RTDE/Vector6D.ctl"/>
		<Item Name="VECTOR6INT32.ctl" Type="VI" URL="../URScript/RTDE/VECTOR6INT32.ctl"/>
		<Item Name="VECTOR6UINT32.ctl" Type="VI" URL="../URScript/RTDE/VECTOR6UINT32.ctl"/>
	</Item>
	<Item Name="CurrentPose.vi" Type="VI" URL="../CurrentPose.vi"/>
	<Item Name="Global 1.vi" Type="VI" URL="../URScript/Global 1.vi"/>
</Library>
