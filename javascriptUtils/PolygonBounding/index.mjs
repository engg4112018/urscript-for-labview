import {GeoPoint, GeoPolygon, GeoPolygonProc, GeoPlane} from "./GeoProc/GeoProc"

function test(){
    var p1 = new GeoPoint( - 27.28046,  37.11775,  - 39.03485);
    var p2 = new GeoPoint( - 44.40014,  38.50727,  - 28.78860);
    var p3 = new GeoPoint( - 49.63065,  20.24757,  - 35.05160);
    var p4 = new GeoPoint( - 32.51096,  18.85805,  - 45.29785);
    var p5 = new GeoPoint( - 23.59142,  10.81737,  - 29.30445);
    var p6 = new GeoPoint( - 18.36091,  29.07707,  - 23.04144);
    var p7 = new GeoPoint( - 35.48060,  30.46659,  - 12.79519);
    var p8 = new GeoPoint( - 40.71110,  12.20689,  - 19.05819);
    var gp = [p1, p2, p3, p4, p5, p6, p7, p8];
    var gpInst = new GeoPolygon(gp);
    var gppInst = new GeoPolygonProc(gpInst);
    var str = '';
    str = str + gppInst.MaxDisError + '\r';
    str = str + gppInst.x0 + ',' + gppInst.x1 + ',' + gppInst.y0 + ',' + gppInst.y1 + ',' + gppInst.y0 + ',' + gppInst.y1 + '\r';
    str = str + gppInst.NumberOfFaces + '\r';
    for (var i = 0; i < gppInst.NumberOfFaces; i ++ )
    {
    str += gppInst.FacePlanes[i].a + ',' + gppInst.FacePlanes[i].b + ',' +
    gppInst.FacePlanes[i].c + ',' + gppInst.FacePlanes[i].d + '\r';
    }
    var insidePoint = new GeoPoint( - 28.411750,     25.794500,      - 37.969000);
    var outsidePoint = new GeoPoint( - 28.411750,      25.794500,      - 50.969000);
    console.log(gppInst.pointInside3DPolygon(insidePoint.x, insidePoint.y, insidePoint.z))
    console.log(gppInst.pointInside3DPolygon(outsidePoint.x, outsidePoint.y, outsidePoint.z))
    console.log(str)
}

function dif(){
    var p1 = new GeoPoint( - 27.28046,  37.11775,  - 39.03485);
    var p2 = new GeoPoint( - 44.40014,  38.50727,  - 28.78860);
    var p3 = new GeoPoint( - 49.63065,  20.24757,  - 35.05160);
    var geoTest = GeoPlane.create(p1, p2, p3)
    console.log(geoTest)
}

//dif()

test()