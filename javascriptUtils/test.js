const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

console.log("initialised!")
rl.on('line', (input) => {
    console.log(`Received: ${input}`);
});