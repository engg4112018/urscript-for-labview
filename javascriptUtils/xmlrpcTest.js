var xmlrpc = require('xmlrpc')
 
// Creates an XML-RPC server to listen to XML-RPC method calls
var server = xmlrpc.createServer({ host: '192.168.1.3', port: 80 })
// Handle methods not found
server.on('NotFound', function(method, params) {
  console.log('Method ' + method + ' does not exist');
})
// Handle method calls by listening for events with the method call name
server.on('post', function (err, params, callback) {
  console.log('Method call params for \'anAction\': ' + params)
 
  // ...perform an action...
 
  // Send a method response with a value
  callback(null, [0,0,1,0,0,0])
})

server.on('isConnected', function(err,params,callback) {
    console.log("=======================")
    callback(null, false)
})
console.log('XML-RPC server listening on port 9091')

// var client = xmlrpc.createClient({ host: '192.168.1.3', port: 8001, path: '/RPC/UR5RPC/'})
 
// // Sends a method call to the XML-RPC server
// client.methodCall('isConnected', [], function (error, value) {
//   // Results of the method response
//   console.log('Method response for \'anAction\': ' + value)
// })