import axios from "axios"

class UR5Service {
    constructor(){
        const url = `http://127.0.0.1:3000/hlmm/`
        axios.defaults.baseURL = url
    }

    async moveJoints(joints){
        try{
            const result = await axios.post("moveJoints", joints)
            await this.waitUntilMoveFinished()
            return result.data
        }catch(err){
            console.log("err", err)
        }
    }

    async lookAt(enable,coords){
        try{
            const result = await axios.post(`lookAt?enable=${enable?"true":"false"}`, coords)
            await this.waitUntilMoveFinished()
            return result.data
        }catch(err){
            console.log("err", err)
        }
    }

    async translate(coords){
        try{
            const result = await axios.post("translate", coords)
            await this.waitUntilMoveFinished()
            return result.data
        }catch(err){
            console.log("err", err)
        }
    }

    async toPos(coords){
        try{
            const result = await axios.post("toPos", coords)
            await this.waitUntilMoveFinished()
            return result.data
        }catch(err){
            console.log("err", err)
        }
    }

    async readyForCommand(){
        try{
            const result = await axios.get("status")
            const data = result.data
            return !data.ProgramRunning
        }catch(err){
            console.log("err", err)
        }
    }
    async getCurrentPose(){
        try{
            const result = await axios.get("status")
            const data = result.data
            return data.CurrentPose
        }catch(err){
            console.log("err", err)
        }
    }
    async setPseudoFreedrivePolygon(points){
        try{
            const result = await axios.post("setPseudoFreedrivePolygon", points)
            const data = result.data
            return data
        }catch(err){
            console.log("err", err)
        }
    }
    async runPsuedoFreedriveMode(){
        try{
            const result = await axios.post("runPsuedoFreedriveMode")
            const data = result.data
            return data
        }catch(err){
            console.log("err", err)
        }
    }
    async waitUntilMoveFinished(){
        try{
            let ready = await this.readyForCommand()
            while(!ready){
                await this.wait(20)
                ready = await this.readyForCommand()
            }
            return ready
        }catch(err){
            console.log("err", err)
        }
    }
    wait(num){
        return new Promise((res) => {
            setTimeout(res,num)
        })
    }
}
let instance = false

function getUR5Service(){
    if(!instance){
        instance = new UR5Service()
    }
    return instance
}

export default getUR5Service()