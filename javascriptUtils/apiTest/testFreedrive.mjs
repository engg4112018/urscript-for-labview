import UR5Service from "./UR5Service"

async function testFreedrive(){
    const currentPose = {x:0, y:0, z:0}//await UR5Service.getCurrentPose()
    console.log(currentPose)
    const {x,y,z} = currentPose
    const topx = x+0.2
    const bottomx = x-0.2
    const xPoint = [
        topx,
        topx,
        topx,
        topx,
        bottomx,
        bottomx,
        bottomx,
        bottomx
    ]
    const yTop = y+0.2
    const yBottom = y-0.2
    const yPoint = [
        yTop,
        yTop,
        yBottom,
        yBottom,
        yTop,
        yTop,
        yBottom,
        yBottom
    ]
    const zTop = z+0.2
    const zTopMid = z+0.2
    const zBottomMid = z-0.2
    const zBottom = z-0.2
    const zPoint = [
        zTopMid,
        zBottomMid,
        zTopMid,
        zBottomMid,
        zTopMid,
        zBottomMid,
        zTopMid,
        zBottomMid
    ]
    const points = {
        x:xPoint,
        y:yPoint,
        z:zPoint
    }
    console.log(points)
    //const polyRes = await UR5Service.setPseudoFreedrivePolygon(points)
    //const runRes = await UR5Service.runPsuedoFreedriveMode()
    //console.log("polyRes",polyRes)
    //console.log("runRes", runRes)
}

testFreedrive()