import UR5Service from "./UR5Service"

async function doMove(){
    let jointJSON = {
        "Base":90,
        "Shoulder":-90,
        "Elbow":90,
        "Wrist 1":180,
        "Wrist 2":-90,
        "Wrist 3":0
    }
    let result = await UR5Service.moveJoints(jointJSON)
    await waitUntilMoveFinished()
    jointJSON.Base = 0
    await UR5Service.moveJoints(jointJSON)
}


async function doLook(){
    const currentPose = await UR5Service.getCurrentPose()
    // const coord = {x:currentPose.x +0.1, y:currentPose.y, z:currentPose.z}
    // await UR5Service.toPos(coord)
    //currentPose.x = currentPose.x - 0.2
    await UR5Service.lookAt(true, {x:currentPose.x-0.1, y:currentPose.y, z:currentPose.z})
    for(var i = 0; i < 5; i++){
        await UR5Service.translate({x:0, y:-0.1, z:0})
        await UR5Service.translate({x:0, y:0.2, z:0})
        await UR5Service.translate({x:0, y:-0.1, z:0})
        await UR5Service.translate({x:0, y:0, z:-0.1})
        await UR5Service.translate({x:0, y:0, z:0.2})
        await UR5Service.translate({x:0, y:0, z:-0.1})
    }
    await UR5Service.lookAt(false, {})
}



//doMove()

doLook()