var net = require('net');
var struct = require("python-struct")
var zmq = require("zeromq")
var sock = zmq.socket("pub")
sock.bindSync('tcp://127.0.0.1:3000')
var client = new net.Socket();

function init(){
    client.connect(30003, '10.0.2.15', function() {
        console.log("connected")
    });
}



let pCoords = {
    x:undefined, 
    y: undefined, 
    z: undefined, 
    rx: undefined,
    ry: undefined,
    rz: undefined, 
}

function determineMove(x, y, z, rx, ry, rz){
    if(
        pCoords.x !== x ||
        pCoords.y !== y ||
        pCoords.z !== z ||
        pCoords.rx !== rx ||
        pCoords.ry !== ry ||
        pCoords.rz !== rz 
    ){
        console.log(`
            x: ${x}
            y: ${y}
            z: ${z}
            rx: ${rx}
            ry: ${ry}
            rz: ${rz}
        `)
        const data = {
            x:x,
            y:y,
            z:z,
            rx:rx,
            ry:ry,
            rz:rz
        }
        sock.send(JSON.stringify(data))
    }
}


client.on("data", (chunk) => {
    const data = []
    data.push(...chunk)
    readMetrics(data)
})

function readMetrics(data){
    let size = read(data, 4)
    const packetSize = size.readUInt32BE()
    let packet_2 = read(data, 8)
    let packet_3 = read(data, 48)

    let packet_4 = read(data, 48)
    let packet_5 = read(data, 48)
    let packet_6 = read(data, 48)
    let packet_7 = read(data, 48)
    let packet_8 = read(data, 48)
    let packet_9 = read(data, 48)
    let packet_10 = read(data, 48)
    let packet_11 = read(data, 48)
    
    let packet_12 = read(data, 8)
    let x = struct.unpack('!d', packet_12)[0]

    let packet_13 = read(data, 8)
    let y = struct.unpack('!d', packet_13)[0]

    let packet_14 = read(data, 8)
    let z = struct.unpack('!d', packet_14)[0]

    let packet_15 = read(data, 8)
    let rx = struct.unpack('!d', packet_15)[0]

    let packet_16 = read(data, 8)
    let ry = struct.unpack('!d', packet_16)[0]

    let packet_17 = read(data, 8)
    let rz = struct.unpack('!d', packet_17)[0]
    determineMove(x, y, z, rx, ry, rz)
}

function read(data, num){
    return new Buffer(data.splice(0,num))
}



client.on("end", () => {
    init()
})


//read(data, )

init()