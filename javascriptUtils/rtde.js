var struct = require("python-struct")
var net = require('net');
var client = new net.Socket();

var cmd = 79
var arr = []
var cb
var setup = false
var outputSize = 0 
function sendOutputSetup(variables, types=[], frequency=125){
    let payload = variables.join(',')
    console.log("payload", payload)
    sendAndRecieve(cmd, payload, (data) => {
        arr = data.split(",")
        arr.forEach(element => {
            if(element === "VECTOR6D"){
                outputSize = outputSize + 6*64
            }
        });
        console.log(outputSize)
        setup = true
        send(83)
    })
}

function send(cmd){
    let fmt = '>HB'
    let size = struct.sizeOf(fmt)
    let buf = struct.pack(fmt, size, cmd)
    client.write(buf)
}

function sendAndRecieve(cmd, payload='', c){
    cb = c
    let fmt = '>HB'
    console.log("size", struct.sizeOf(fmt))
    let size = struct.sizeOf(fmt) + payload.length
    console.log("payload.length", payload.length)
    console.log("actual size", size)
    console.log(struct.pack(fmt, size, cmd).toString())
    let buf = struct.pack(fmt, size, cmd) + payload
    console.log(struct.pack(fmt, size, cmd).toString(),"here")
    client.write(buf)
}

function init(){
    client.connect(30004, '169.254.163.200', function() {
        console.log("connected")
        sendOutputSetup(["actual_q","actual_TCP_pose"])
        //sendAndRecieve(118)
    });

    client.on("data", (chunk) => {
        const data = []
        data.push(...chunk)
        if(setup){
            test(data)
        }else{
            readMetrics(data)
        }
    })
}

function test(data){
    //console.log("data.length",data.length)
    if(data.length === 4){
        var z = new Buffer(data)
        console.log("size? n iugiub", z.readUInt32BE())
    }else{
        let size = read(data, 2).readUInt16BE()
        let command = read(data, 1).readUInt8()
        console.log("size?", size)
        console.log("command?", command)
        readJoint(data)
        readJoint(data)
        console.log("length",data.length)
    }
}

function readJoint(data){
    let packet_12 = read(data, 8)
    let x = struct.unpack('!d', packet_12)[0]

    let packet_13 = read(data, 8)
    let y = struct.unpack('!d', packet_13)[0]

    let packet_14 = read(data, 8)
    let z = struct.unpack('!d', packet_14)[0]

    let packet_15 = read(data, 8)
    let rx = struct.unpack('!d', packet_15)[0]

    let packet_16 = read(data, 8)
    let ry = struct.unpack('!d', packet_16)[0]

    let packet_17 = read(data, 8)
    let rz = struct.unpack('!d', packet_17)[0]
    console.log(x,y,z,rx,ry,rz)
}

function read(data, num){
    return new Buffer(data.splice(0,num))
}

function unpack(data){
    //let id = read(data, 1).readUInt8()
    //console.log("id", id)
    //let types = data.split(',')
    let t = new Buffer(data)
    console.log("test", t.toString())
    console.log("types", data)
    cb(t.toString())
}

function readMetrics(data){
    let size = read(data, 2).readUInt16BE()
    let command = read(data, 1).readUInt8()
    //let test = struct.unpack('>HB', data)
    console.log("size", size)
    console.log("command", command)
    unpack(data)
}


init()