
 EPSILON = 0.0000001
 MODULUS = (p) => (Math.sqrt(p.x*p.x + p.y*p.y + p.z*p.z))
 TWOPI = 6.283185307179586476925287
 RTOD =  57.2957795
 
 function CalcAngleSum(q, p, n)
 {
    var i;
    var m1,m2;
    var anglesum=0, costheta;
    var p1 = {x:0,y:0, z:0}
    var p2 = {x:0,y:0, z:0}
    for (i=0;i<n;i++) {
 
       p1.x = p[i].x - q.x;
       p1.y = p[i].y - q.y;
       p1.z = p[i].z - q.z;
       p2.x = p[(i+1)%n].x - q.x;
       p2.y = p[(i+1)%n].y - q.y;
       p2.z = p[(i+1)%n].z - q.z;
 
       m1 = MODULUS(p1);
       m2 = MODULUS(p2);
       //console.log(m1,m2)
       if (m1*m2 <= EPSILON)
          return(TWOPI); /* We are on a node, consider this inside */
       else
          costheta = (p1.x*p2.x + p1.y*p2.y + p1.z*p2.z) / (m1*m2);
        //console.log(costheta)
        //console.log(Math.acos(-1.00000000000002))
       anglesum += Math.acos(costheta);
       //console.log(anglesum)
    }
    return(anglesum);
 }


 const polygon = [
     {x:-2,y:-2,z:2},{x:-2,y:-2,z:-2},
     {x:-2,y:2,z:2},{x:-2,y:-2,z:2},
     {x:-2,y:2,z:2},{x:2,y:2,z:2},
     {x:-2,y:2,z:2},{x:-2,y:2,z:-2},
     {x:2,y:-2,z:-2},{x:-2,y:-2,z:-2},
     {x:2,y:-2,z:-2},{x:2,y:-2,z:2},
     {x:2,y:-2,z:-2},{x:2,y:2,z:-2},
     {x:2,y:2,z:-2},{x:-2,y:2,z:-2},
     {x:2,y:2,z:-2},{x:2,y:2,z:2},
     {x:-2,y:2,z:-2},{x:-2,y:-2,z:-2},
     {x:2,y:2,z:2},{x:2,y:-2,z:2},
     {x:-2,y:-2,z:2},{x:2,y:-2,z:2},
 ]

 const point = {
     x:0,
     y:0,
     z:0
 }

 console.log(CalcAngleSum(point, polygon, 8))