# API End Points

All Endpoints are appended to the route /hlmm/

## /home
### HTTP Method
Post
### Query Parameters
None
### Request Body
None

### Response Body
```
{
    Success:Boolean,
    Error: {
        status: Boolean,
        code: Integer,
        source: String
    }
}
```
## /lookAt
### HTTP Method
Post
### Query Parameters
enable="true/false"
### Request Body
```
{
    x: float,
    y: float,
    z: float
}
```

### Response Body
```
{
    Success:Boolean,
    Error: {
        status: Boolean,
        code: Integer,
        source: String
    }
}
```

## /moveJoints
### HTTP Method
Post
### Query Parameters
None
### Request Body
```
{
    Base: float,
    Shoulder: float,
    Elbow: float,
    "Wrist 1": float,
    "Wrist 2": float,
    "Wrist 3": float
}
```

## /status
### HTTP Method
Get
### Query Parameters
None
### Request Body
None

### Response Body
```
{
    PowerOn:Boolean,
    ProgramRunning: Boolean,
    PowerButtonPressed: Boolean,
    LookAtEnabled: Boolean,
    CurrentPose:{
        x: double,
        y: double,
        z: double,
        rx: double,
        ry: double,
        rz: double
    },
    JointPosition:{
        Base: double,
        Shoulder: double,
        Elbow: double,
        "Wrist 1": double,
        "Wrist 2": double,
        "Wrist 3": double
    }
}
```

## /translate
### HTTP Method
Post
### Query Parameters
None
### Request Body
```
{
    x: float,
    y: float,
    z: float
}
```

### Response Body
```
{
    Success:Boolean,
    Error: {
        status: Boolean,
        code: Integer,
        source: String
    }
}
```

## /toPos
### HTTP Method
Post
### Query Parameters
None
### Request Body
```
{
    x: float,
    y: float,
    z: float
}
```

### Response Body
```
{
    Success:Boolean,
    Error: {
        status: Boolean,
        code: Integer,
        source: String
    }
}
```

## /stop
### HTTP Method
Post
### Query Parameters
None
### Request Body
None

### Response Body
```
{
    Success:Boolean,
    Error: {
        status: Boolean,
        code: Integer,
        source: String
    }
}
```

## /setPseudoFreedrivePolygon
### HTTP Method
Post
### Query Parameters
None
### Request Body
```
{
    x: Array[double],
    y: Array[double],
    z: Array[double]
}
```

### Response Body
```
{
    Success:Boolean,
    Error: {
        status: Boolean,
        code: Integer,
        source: String
    }
}
```

## /runPseudoFreedriveMode
### HTTP Method
Post
### Query Parameters
None
### Request Body
None

### Response Body
```
{
    Success:Boolean,
    Error: {
        status: Boolean,
        code: Integer,
        source: String
    }
}
```

## /endPseudoFreedriveMode
### HTTP Method
Post
### Query Parameters
None
### Request Body
None

### Response Body
```
{
    Success:Boolean,
    Error: {
        status: Boolean,
        code: Integer,
        source: String
    }
}
```
