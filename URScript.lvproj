﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="HighLevelMovementManager" Type="Folder">
			<Item Name="Controls" Type="Folder">
				<Item Name="BOOL.ctl" Type="VI" URL="../URScript/RTDE/BOOL.ctl"/>
				<Item Name="DOUBLE.ctl" Type="VI" URL="../URScript/RTDE/DOUBLE.ctl"/>
				<Item Name="INT32.ctl" Type="VI" URL="../URScript/RTDE/INT32.ctl"/>
				<Item Name="JointRotation.ctl" Type="VI" URL="../HighLevelMovementManager/Movement/JointRotation.ctl"/>
				<Item Name="Translate.ctl" Type="VI" URL="../HighLevelMovementManager/Movement/Translate.ctl"/>
			</Item>
			<Item Name="MovementManager" Type="Folder">
				<Item Name="DisableLook.vi" Type="VI" URL="../HighLevelMovementManager/Movement/DisableLook.vi"/>
				<Item Name="EnableLook.vi" Type="VI" URL="../HighLevelMovementManager/Movement/EnableLook.vi"/>
				<Item Name="EndMovement.vi" Type="VI" URL="../HighLevelMovementManager/WebService/MovementManager/EndMovement.vi"/>
				<Item Name="GetRobotStatus.vi" Type="VI" URL="../HighLevelMovementManager/Utils/GetRobotStatus.vi"/>
				<Item Name="Look.vi" Type="VI" URL="../HighLevelMovementManager/Movement/Look.vi"/>
				<Item Name="OverallStatus.vi" Type="VI" URL="../HighLevelMovementManager/OverallStatus.vi"/>
				<Item Name="PseudoFreedriveMode.vi" Type="VI" URL="../HighLevelMovementManager/Movement/PseudoFreedriveMode.vi"/>
				<Item Name="RotateJoints.vi" Type="VI" URL="../HighLevelMovementManager/Movement/RotateJoints.vi"/>
				<Item Name="StopPseudoFreedriveMode.vi" Type="VI" URL="../HighLevelMovementManager/Movement/StopPseudoFreedriveMode.vi"/>
				<Item Name="TargetPoint.ctl" Type="VI" URL="../../../URScript-for-LabVIEW/utils/TargetPoint.ctl"/>
				<Item Name="ToCoord.vi" Type="VI" URL="../HighLevelMovementManager/Movement/ToCoord.vi"/>
				<Item Name="ToHome.vi" Type="VI" URL="../HighLevelMovementManager/Movement/ToHome.vi"/>
				<Item Name="TranslatePose.vi" Type="VI" URL="../HighLevelMovementManager/Movement/TranslatePose.vi"/>
			</Item>
			<Item Name="PolygonBounding" Type="Folder">
				<Item Name="GeoFace.lvclass" Type="LVClass" URL="../HighLevelMovementManager/PolygonBounding/GeoFace/GeoFace.lvclass"/>
				<Item Name="GeoPlane.lvclass" Type="LVClass" URL="../HighLevelMovementManager/PolygonBounding/GeoPlane/GeoPlane.lvclass"/>
				<Item Name="GeoPoint.lvclass" Type="LVClass" URL="../HighLevelMovementManager/PolygonBounding/GeoPoint/GeoPoint.lvclass"/>
				<Item Name="GeoPolygon.lvclass" Type="LVClass" URL="../HighLevelMovementManager/PolygonBounding/GeoPolygon/GeoPolygon.lvclass"/>
				<Item Name="GeoPolygonProc.lvclass" Type="LVClass" URL="../HighLevelMovementManager/PolygonBounding/GeoPolygonProc/GeoPolygonProc.lvclass"/>
				<Item Name="GeoVector.lvclass" Type="LVClass" URL="../HighLevelMovementManager/PolygonBounding/GeoVector/GeoVector.lvclass"/>
				<Item Name="Utility.lvclass" Type="LVClass" URL="../HighLevelMovementManager/PolygonBounding/Utility/Utility.lvclass"/>
			</Item>
			<Item Name="RemoteProcedureControl" Type="Folder">
				<Item Name="methods" Type="Folder">
					<Item Name="MovementManager" Type="Folder">
						<Item Name="utils" Type="Folder">
							<Item Name="ConstructResponse.vi" Type="VI" URL="../HighLevelMovementManager/WebService/MovementManager/utils/ConstructResponse.vi"/>
							<Item Name="SendResponse.vi" Type="VI" URL="../HighLevelMovementManager/WebService/MovementManager/utils/SendResponse.vi"/>
							<Item Name="WaitUntilMovementRegistered.vi" Type="VI" URL="../HighLevelMovementManager/WebService/MovementManager/utils/WaitUntilMovementRegistered.vi"/>
						</Item>
						<Item Name="MovementCommandResponse.ctl" Type="VI" URL="../HighLevelMovementManager/WebService/MovementManager/MovementCommandResponse.ctl"/>
					</Item>
					<Item Name="RPC" Type="Folder">
						<Item Name="utils" Type="Folder">
							<Item Name="returnXML" Type="Folder">
								<Item Name="ReturnBooleanXML.vi" Type="VI" URL="../HighLevelMovementManager/WebService/RPC/ReturnBooleanXML.vi"/>
								<Item Name="ReturnDoubleArrayXML.vi" Type="VI" URL="../HighLevelMovementManager/WebService/RPC/ReturnDoubleArrayXML.vi"/>
								<Item Name="ReturnIntArrayXML.vi" Type="VI" URL="../HighLevelMovementManager/WebService/RPC/ReturnIntArrayXML.vi"/>
							</Item>
							<Item Name="ExtractMethodName.vi" Type="VI" URL="../HighLevelMovementManager/WebService/RPC/ExtractMethodName.vi"/>
							<Item Name="InitXML.vi" Type="VI" URL="../HighLevelMovementManager/WebService/RPC/InitXML.vi"/>
							<Item Name="ReturnXML.vi" Type="VI" URL="../HighLevelMovementManager/WebService/RPC/ReturnXML.vi"/>
							<Item Name="RunMethod.vi" Type="VI" URL="../HighLevelMovementManager/WebService/RPC/RunMethod.vi"/>
						</Item>
						<Item Name="axisReset.vi" Type="VI" URL="../HighLevelMovementManager/WebService/RPC/axisReset.vi"/>
						<Item Name="getAxisSpeed.vi" Type="VI" URL="../HighLevelMovementManager/WebService/RPC/getAxisSpeed.vi"/>
						<Item Name="getForceParams.vi" Type="VI" URL="../HighLevelMovementManager/WebService/RPC/getForceParams.vi"/>
						<Item Name="getWrench.vi" Type="VI" URL="../HighLevelMovementManager/WebService/RPC/getWrench.vi"/>
						<Item Name="IsConnected.vi" Type="VI" URL="../HighLevelMovementManager/WebService/RPC/IsConnected.vi"/>
					</Item>
				</Item>
				<Item Name="New Folder" Type="Folder"/>
				<Item Name="hlmm" Type="Web Service">
					<Property Name="ws.autoIncrementVersion" Type="Bool">true</Property>
					<Property Name="ws.disconnectInline" Type="Bool">true</Property>
					<Property Name="ws.disconnectTypeDefs" Type="Bool">false</Property>
					<Property Name="ws.guid" Type="Str">{AC9ECB70-2351-4C32-83D7-6A602EF7B4CA}</Property>
					<Property Name="ws.modifyLibraryFile" Type="Bool">true</Property>
					<Property Name="ws.remoteDebugging" Type="Bool">true</Property>
					<Property Name="ws.removeLibraryItems" Type="Bool">true</Property>
					<Property Name="ws.removePolyVIs" Type="Bool">true</Property>
					<Property Name="ws.serveDefaultDoc" Type="Bool">true</Property>
					<Property Name="ws.SSE2" Type="Bool">false</Property>
					<Property Name="ws.static_permissions" Type="Str"></Property>
					<Property Name="ws.version.build" Type="Int">0</Property>
					<Property Name="ws.version.fix" Type="Int">0</Property>
					<Property Name="ws.version.major" Type="Int">1</Property>
					<Property Name="ws.version.minor" Type="Int">0</Property>
					<Item Name="Startup VIs" Type="Startup VIs Container">
						<Item Name="InitialiseRTDE.vi" Type="VI" URL="../HighLevelMovementManager/WebService/Startup/InitialiseRTDE.vi">
							<Property Name="ws.type" Type="Int">2</Property>
						</Item>
					</Item>
					<Item Name="Web Resources" Type="HTTP WebResources Container">
						<Item Name="endPsuedoFreedriveMode.vi" Type="VI" URL="../HighLevelMovementManager/WebService/MovementManager/endPsuedoFreedriveMode.vi">
							<Property Name="ws.buffered" Type="Bool">true</Property>
							<Property Name="ws.includeNameInURL" Type="Bool">true</Property>
							<Property Name="ws.keepInMemory" Type="Bool">true</Property>
							<Property Name="ws.loadAtStartup" Type="Bool">true</Property>
							<Property Name="ws.method" Type="Int">3</Property>
							<Property Name="ws.outputFormat" Type="Int">2</Property>
							<Property Name="ws.outputType" Type="Int">1</Property>
							<Property Name="ws.permissions" Type="Str"></Property>
							<Property Name="ws.requireAPIKey" Type="Bool">false</Property>
							<Property Name="ws.type" Type="Int">1</Property>
							<Property Name="ws.uri" Type="Str"></Property>
							<Property Name="ws.useHeaders" Type="Bool">true</Property>
							<Property Name="ws.useStandardURL" Type="Bool">true</Property>
						</Item>
						<Item Name="home.vi" Type="VI" URL="../HighLevelMovementManager/WebService/MovementManager/home.vi">
							<Property Name="ws.buffered" Type="Bool">true</Property>
							<Property Name="ws.includeNameInURL" Type="Bool">true</Property>
							<Property Name="ws.keepInMemory" Type="Bool">true</Property>
							<Property Name="ws.loadAtStartup" Type="Bool">true</Property>
							<Property Name="ws.method" Type="Int">3</Property>
							<Property Name="ws.outputFormat" Type="Int">2</Property>
							<Property Name="ws.outputType" Type="Int">1</Property>
							<Property Name="ws.permissions" Type="Str"></Property>
							<Property Name="ws.requireAPIKey" Type="Bool">false</Property>
							<Property Name="ws.type" Type="Int">1</Property>
							<Property Name="ws.uri" Type="Str"></Property>
							<Property Name="ws.useHeaders" Type="Bool">true</Property>
							<Property Name="ws.useStandardURL" Type="Bool">true</Property>
						</Item>
						<Item Name="lookAt.vi" Type="VI" URL="../HighLevelMovementManager/WebService/MovementManager/lookAt.vi">
							<Property Name="ws.buffered" Type="Bool">true</Property>
							<Property Name="ws.includeNameInURL" Type="Bool">true</Property>
							<Property Name="ws.keepInMemory" Type="Bool">true</Property>
							<Property Name="ws.loadAtStartup" Type="Bool">true</Property>
							<Property Name="ws.method" Type="Int">3</Property>
							<Property Name="ws.outputFormat" Type="Int">2</Property>
							<Property Name="ws.outputType" Type="Int">1</Property>
							<Property Name="ws.permissions" Type="Str"></Property>
							<Property Name="ws.requireAPIKey" Type="Bool">false</Property>
							<Property Name="ws.type" Type="Int">1</Property>
							<Property Name="ws.uri" Type="Str"></Property>
							<Property Name="ws.useHeaders" Type="Bool">true</Property>
							<Property Name="ws.useStandardURL" Type="Bool">true</Property>
						</Item>
						<Item Name="moveJoints.vi" Type="VI" URL="../HighLevelMovementManager/WebService/MovementManager/moveJoints.vi">
							<Property Name="ws.buffered" Type="Bool">true</Property>
							<Property Name="ws.includeNameInURL" Type="Bool">true</Property>
							<Property Name="ws.keepInMemory" Type="Bool">true</Property>
							<Property Name="ws.loadAtStartup" Type="Bool">true</Property>
							<Property Name="ws.method" Type="Int">3</Property>
							<Property Name="ws.outputFormat" Type="Int">2</Property>
							<Property Name="ws.outputType" Type="Int">1</Property>
							<Property Name="ws.permissions" Type="Str"></Property>
							<Property Name="ws.requireAPIKey" Type="Bool">false</Property>
							<Property Name="ws.type" Type="Int">1</Property>
							<Property Name="ws.uri" Type="Str"></Property>
							<Property Name="ws.useHeaders" Type="Bool">true</Property>
							<Property Name="ws.useStandardURL" Type="Bool">true</Property>
						</Item>
						<Item Name="runPsuedoFreedriveMode.vi" Type="VI" URL="../HighLevelMovementManager/WebService/MovementManager/runPsuedoFreedriveMode.vi">
							<Property Name="ws.buffered" Type="Bool">true</Property>
							<Property Name="ws.includeNameInURL" Type="Bool">true</Property>
							<Property Name="ws.keepInMemory" Type="Bool">true</Property>
							<Property Name="ws.loadAtStartup" Type="Bool">true</Property>
							<Property Name="ws.method" Type="Int">3</Property>
							<Property Name="ws.outputFormat" Type="Int">2</Property>
							<Property Name="ws.outputType" Type="Int">1</Property>
							<Property Name="ws.permissions" Type="Str"></Property>
							<Property Name="ws.requireAPIKey" Type="Bool">false</Property>
							<Property Name="ws.type" Type="Int">1</Property>
							<Property Name="ws.uri" Type="Str"></Property>
							<Property Name="ws.useHeaders" Type="Bool">true</Property>
							<Property Name="ws.useStandardURL" Type="Bool">true</Property>
						</Item>
						<Item Name="setPseudoFreedrivePolygon.vi" Type="VI" URL="../HighLevelMovementManager/WebService/MovementManager/setPseudoFreedrivePolygon.vi">
							<Property Name="ws.buffered" Type="Bool">true</Property>
							<Property Name="ws.includeNameInURL" Type="Bool">true</Property>
							<Property Name="ws.keepInMemory" Type="Bool">true</Property>
							<Property Name="ws.loadAtStartup" Type="Bool">true</Property>
							<Property Name="ws.method" Type="Int">3</Property>
							<Property Name="ws.outputFormat" Type="Int">2</Property>
							<Property Name="ws.outputType" Type="Int">1</Property>
							<Property Name="ws.permissions" Type="Str"></Property>
							<Property Name="ws.requireAPIKey" Type="Bool">false</Property>
							<Property Name="ws.type" Type="Int">1</Property>
							<Property Name="ws.uri" Type="Str"></Property>
							<Property Name="ws.useHeaders" Type="Bool">true</Property>
							<Property Name="ws.useStandardURL" Type="Bool">true</Property>
						</Item>
						<Item Name="status.vi" Type="VI" URL="../HighLevelMovementManager/WebService/MovementManager/status.vi">
							<Property Name="ws.buffered" Type="Bool">true</Property>
							<Property Name="ws.includeNameInURL" Type="Bool">true</Property>
							<Property Name="ws.keepInMemory" Type="Bool">true</Property>
							<Property Name="ws.loadAtStartup" Type="Bool">true</Property>
							<Property Name="ws.method" Type="Int">1</Property>
							<Property Name="ws.outputFormat" Type="Int">2</Property>
							<Property Name="ws.outputType" Type="Int">1</Property>
							<Property Name="ws.permissions" Type="Str"></Property>
							<Property Name="ws.requireAPIKey" Type="Bool">false</Property>
							<Property Name="ws.type" Type="Int">1</Property>
							<Property Name="ws.uri" Type="Str"></Property>
							<Property Name="ws.useHeaders" Type="Bool">true</Property>
							<Property Name="ws.useStandardURL" Type="Bool">true</Property>
						</Item>
						<Item Name="stop.vi" Type="VI" URL="../HighLevelMovementManager/WebService/MovementManager/stop.vi">
							<Property Name="ws.buffered" Type="Bool">true</Property>
							<Property Name="ws.includeNameInURL" Type="Bool">true</Property>
							<Property Name="ws.keepInMemory" Type="Bool">true</Property>
							<Property Name="ws.loadAtStartup" Type="Bool">true</Property>
							<Property Name="ws.method" Type="Int">3</Property>
							<Property Name="ws.outputFormat" Type="Int">2</Property>
							<Property Name="ws.outputType" Type="Int">1</Property>
							<Property Name="ws.permissions" Type="Str"></Property>
							<Property Name="ws.requireAPIKey" Type="Bool">false</Property>
							<Property Name="ws.type" Type="Int">1</Property>
							<Property Name="ws.uri" Type="Str"></Property>
							<Property Name="ws.useHeaders" Type="Bool">true</Property>
							<Property Name="ws.useStandardURL" Type="Bool">true</Property>
						</Item>
						<Item Name="toPos.vi" Type="VI" URL="../HighLevelMovementManager/WebService/MovementManager/toPos.vi">
							<Property Name="ws.buffered" Type="Bool">true</Property>
							<Property Name="ws.includeNameInURL" Type="Bool">true</Property>
							<Property Name="ws.keepInMemory" Type="Bool">true</Property>
							<Property Name="ws.loadAtStartup" Type="Bool">true</Property>
							<Property Name="ws.method" Type="Int">3</Property>
							<Property Name="ws.outputFormat" Type="Int">2</Property>
							<Property Name="ws.outputType" Type="Int">1</Property>
							<Property Name="ws.permissions" Type="Str"></Property>
							<Property Name="ws.requireAPIKey" Type="Bool">false</Property>
							<Property Name="ws.type" Type="Int">1</Property>
							<Property Name="ws.uri" Type="Str"></Property>
							<Property Name="ws.useHeaders" Type="Bool">true</Property>
							<Property Name="ws.useStandardURL" Type="Bool">true</Property>
						</Item>
						<Item Name="translate.vi" Type="VI" URL="../HighLevelMovementManager/WebService/MovementManager/translate.vi">
							<Property Name="ws.buffered" Type="Bool">true</Property>
							<Property Name="ws.includeNameInURL" Type="Bool">true</Property>
							<Property Name="ws.keepInMemory" Type="Bool">true</Property>
							<Property Name="ws.loadAtStartup" Type="Bool">true</Property>
							<Property Name="ws.method" Type="Int">3</Property>
							<Property Name="ws.outputFormat" Type="Int">2</Property>
							<Property Name="ws.outputType" Type="Int">1</Property>
							<Property Name="ws.permissions" Type="Str"></Property>
							<Property Name="ws.requireAPIKey" Type="Bool">false</Property>
							<Property Name="ws.type" Type="Int">1</Property>
							<Property Name="ws.uri" Type="Str"></Property>
							<Property Name="ws.useHeaders" Type="Bool">true</Property>
							<Property Name="ws.useStandardURL" Type="Bool">true</Property>
						</Item>
						<Item Name="UR5RPC.vi" Type="VI" URL="../HighLevelMovementManager/WebService/Endpoints/URScriptRPC/UR5RPC.vi">
							<Property Name="ws.buffered" Type="Bool">true</Property>
							<Property Name="ws.includeNameInURL" Type="Bool">true</Property>
							<Property Name="ws.keepInMemory" Type="Bool">true</Property>
							<Property Name="ws.loadAtStartup" Type="Bool">true</Property>
							<Property Name="ws.method" Type="Int">3</Property>
							<Property Name="ws.outputFormat" Type="Int">2</Property>
							<Property Name="ws.outputType" Type="Int">1</Property>
							<Property Name="ws.permissions" Type="Str"></Property>
							<Property Name="ws.requireAPIKey" Type="Bool">false</Property>
							<Property Name="ws.type" Type="Int">1</Property>
							<Property Name="ws.uri" Type="Str">RPC/UR5RPC</Property>
							<Property Name="ws.useHeaders" Type="Bool">true</Property>
							<Property Name="ws.useStandardURL" Type="Bool">false</Property>
						</Item>
					</Item>
				</Item>
			</Item>
			<Item Name="Test" Type="Folder">
				<Item Name="axisTest.vi" Type="VI" URL="../HighLevelMovementManager/test/axisTest.vi"/>
				<Item Name="commandLineTest.vi" Type="VI" URL="../HighLevelMovementManager/test/commandLineTest.vi"/>
				<Item Name="ConstructPolygon.vi" Type="VI" URL="../HighLevelMovementManager/test/ConstructPolygon.vi"/>
				<Item Name="ConstructSimpleSquare.vi" Type="VI" URL="../HighLevelMovementManager/test/ConstructSimpleSquare.vi"/>
				<Item Name="ConstructTestDiamond.vi" Type="VI" URL="../test/ConstructTestDiamond.vi"/>
				<Item Name="ConstructTestPolygon.vi" Type="VI" URL="../test/ConstructTestPolygon.vi"/>
				<Item Name="ControllerTest.vi" Type="VI" URL="../HighLevelMovementManager/test/ControllerTest.vi"/>
				<Item Name="ForceModeTest.vi" Type="VI" URL="../HighLevelMovementManager/test/ForceModeTest.vi"/>
				<Item Name="GetForceNoise.vi" Type="VI" URL="../HighLevelMovementManager/test/GetForceNoise.vi"/>
				<Item Name="GetStatus.vi" Type="VI" URL="../HighLevelMovementManager/test/GetStatus.vi"/>
				<Item Name="JointRotationTest.vi" Type="VI" URL="../HighLevelMovementManager/test/JointRotationTest.vi"/>
				<Item Name="polygonLimitTest.vi" Type="VI" URL="../HighLevelMovementManager/test/polygonLimitTest.vi"/>
				<Item Name="RTDE_test.vi" Type="VI" URL="../HighLevelMovementManager/test/RTDE_test.vi"/>
				<Item Name="testEnum.vi" Type="VI" URL="../HighLevelMovementManager/test/testEnum.vi"/>
				<Item Name="TestProgram.vi" Type="VI" URL="../HighLevelMovementManager/test/TestProgram.vi"/>
				<Item Name="TestPseudoFreedriveModevi.vi" Type="VI" URL="../HighLevelMovementManager/test/TestPseudoFreedriveModevi.vi"/>
				<Item Name="txt2URScript.vi" Type="VI" URL="../HighLevelMovementManager/test/txt2URScript.vi"/>
			</Item>
			<Item Name="Utils" Type="Folder">
				<Item Name="AverageTick.vi" Type="VI" URL="../HighLevelMovementManager/Utils/AverageTick.vi"/>
				<Item Name="AxisShouldReset.vi" Type="VI" URL="../HighLevelMovementManager/Utils/AxisShouldReset.vi"/>
				<Item Name="ConstructGeoProc.vi" Type="VI" URL="../HighLevelMovementManager/Utils/ConstructGeoProc.vi"/>
				<Item Name="DetermineAxisCompliance.vi" Type="VI" URL="../HighLevelMovementManager/Utils/DetermineAxisCompliance.vi"/>
				<Item Name="DetermineCompliance.vi" Type="VI" URL="../HighLevelMovementManager/Utils/DetermineCompliance.vi"/>
				<Item Name="DetermineDisplacement.vi" Type="VI" URL="../HighLevelMovementManager/Utils/DetermineDisplacement.vi"/>
				<Item Name="GetAxisForce.vi" Type="VI" URL="../HighLevelMovementManager/Utils/GetAxisForce.vi"/>
				<Item Name="GetForceArray.vi" Type="VI" URL="../HighLevelMovementManager/Utils/GetForceArray.vi"/>
				<Item Name="GetRequestInterval.vi" Type="VI" URL="../HighLevelMovementManager/Utils/GetRequestInterval.vi"/>
				<Item Name="ShouldReset.vi" Type="VI" URL="../HighLevelMovementManager/Utils/ShouldReset.vi"/>
				<Item Name="ToPose.vi" Type="VI" URL="../HighLevelMovementManager/Utils/ToPose.vi"/>
			</Item>
			<Item Name="HighLevelGlobal.vi" Type="VI" URL="../HighLevelMovementManager/HighLevelGlobal.vi"/>
			<Item Name="Initialise.vi" Type="VI" URL="../Initialise.vi"/>
			<Item Name="PseudoFreedriveModeOld.vi" Type="VI" URL="../PseudoFreedriveModeOld.vi"/>
			<Item Name="RTDE Control.ctl" Type="VI" URL="../URScript/RTDE/RTDE Control.ctl"/>
		</Item>
		<Item Name="URScript.lvlib" Type="Library" URL="../URScript.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="user.lib" Type="Folder">
				<Item Name="Array of VData to VArray__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Array of VData to VArray__ogtk.vi"/>
				<Item Name="Array of VData to VCluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Array of VData to VCluster__ogtk.vi"/>
				<Item Name="Array Size(s)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Array Size(s)__ogtk.vi"/>
				<Item Name="Build Error Cluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/error/error.llb/Build Error Cluster__ogtk.vi"/>
				<Item Name="Cluster to Array of VData__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Cluster to Array of VData__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (Bool)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (Bool)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (CDB)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (CSG)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (CTL-REF)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (CTL-REF)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (CXT)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (DBL)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (EXT)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (GEN-REF)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (GEN-REF)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (GObj-REF)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (GObj-REF)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (I8)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (I16)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (I32)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (I64)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (LVObject)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (Path)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (SGL)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (String)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (U8)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (U16)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (U32)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (U64)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (Variant)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (VI-REF)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel (VI-REF)__ogtk.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Conditional Auto-Indexing Tunnel__ogtk.vi"/>
				<Item Name="Current VIs Parent Directory__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Current VIs Parent Directory__ogtk.vi"/>
				<Item Name="Current VIs Parents Ref__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/appcontrol/appcontrol.llb/Current VIs Parents Ref__ogtk.vi"/>
				<Item Name="Get Array Element Default Data__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Array Element Default Data__ogtk.vi"/>
				<Item Name="Get Array Element TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Array Element TD__ogtk.vi"/>
				<Item Name="Get Data Name from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Data Name from TD__ogtk.vi"/>
				<Item Name="Get Data Name__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Data Name__ogtk.vi"/>
				<Item Name="Get Default Data from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Default Data from TD__ogtk.vi"/>
				<Item Name="Get Element TD from Array TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Element TD from Array TD__ogtk.vi"/>
				<Item Name="Get Header from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Header from TD__ogtk.vi"/>
				<Item Name="Get Last PString__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Last PString__ogtk.vi"/>
				<Item Name="Get PString__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get PString__ogtk.vi"/>
				<Item Name="Get Strings from Enum TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Strings from Enum TD__ogtk.vi"/>
				<Item Name="Get Strings from Enum__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Strings from Enum__ogtk.vi"/>
				<Item Name="Get TDEnum from Data__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get TDEnum from Data__ogtk.vi"/>
				<Item Name="Get Variant Attributes__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Variant Attributes__ogtk.vi"/>
				<Item Name="Get Waveform Type Enum from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Waveform Type Enum from TD__ogtk.vi"/>
				<Item Name="Parse String with TDs__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Parse String with TDs__ogtk.vi"/>
				<Item Name="Reorder 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Reorder 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Reorder 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Set Data Name__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Set Data Name__ogtk.vi"/>
				<Item Name="Set Enum String Value__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Set Enum String Value__ogtk.vi"/>
				<Item Name="Sort 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Sort 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Sort 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Sort 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (DBL)__ogtk.vi"/>
				<Item Name="Sort 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (EXT)__ogtk.vi"/>
				<Item Name="Sort 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (I8)__ogtk.vi"/>
				<Item Name="Sort 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (I16)__ogtk.vi"/>
				<Item Name="Sort 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (I32)__ogtk.vi"/>
				<Item Name="Sort 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (I64)__ogtk.vi"/>
				<Item Name="Sort 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (Path)__ogtk.vi"/>
				<Item Name="Sort 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (SGL)__ogtk.vi"/>
				<Item Name="Sort 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (String)__ogtk.vi"/>
				<Item Name="Sort 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (U8)__ogtk.vi"/>
				<Item Name="Sort 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (U16)__ogtk.vi"/>
				<Item Name="Sort 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (U32)__ogtk.vi"/>
				<Item Name="Sort 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (U64)__ogtk.vi"/>
				<Item Name="Sort 2D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (CDB)__ogtk.vi"/>
				<Item Name="Sort 2D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (CSG)__ogtk.vi"/>
				<Item Name="Sort 2D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (CXT)__ogtk.vi"/>
				<Item Name="Sort 2D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (DBL)__ogtk.vi"/>
				<Item Name="Sort 2D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (EXT)__ogtk.vi"/>
				<Item Name="Sort 2D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (I8)__ogtk.vi"/>
				<Item Name="Sort 2D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (I16)__ogtk.vi"/>
				<Item Name="Sort 2D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (I32)__ogtk.vi"/>
				<Item Name="Sort 2D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (I64)__ogtk.vi"/>
				<Item Name="Sort 2D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (Path)__ogtk.vi"/>
				<Item Name="Sort 2D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (SGL)__ogtk.vi"/>
				<Item Name="Sort 2D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (String)__ogtk.vi"/>
				<Item Name="Sort 2D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (U8)__ogtk.vi"/>
				<Item Name="Sort 2D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (U16)__ogtk.vi"/>
				<Item Name="Sort 2D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (U32)__ogtk.vi"/>
				<Item Name="Sort 2D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (U64)__ogtk.vi"/>
				<Item Name="Sort Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort Array__ogtk.vi"/>
				<Item Name="Split Cluster TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Split Cluster TD__ogtk.vi"/>
				<Item Name="Strip Path - Arrays__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Strip Path - Arrays__ogtk.vi"/>
				<Item Name="Strip Path - Traditional__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Strip Path - Traditional__ogtk.vi"/>
				<Item Name="Strip Path__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Strip Path__ogtk.vi"/>
				<Item Name="Trim Whitespace (String Array)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/Trim Whitespace (String Array)__ogtk.vi"/>
				<Item Name="Trim Whitespace (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/Trim Whitespace (String)__ogtk.vi"/>
				<Item Name="Trim Whitespace__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/Trim Whitespace__ogtk.vi"/>
				<Item Name="Type Descriptor Enumeration__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Enumeration__ogtk.ctl"/>
				<Item Name="Type Descriptor Header__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Header__ogtk.ctl"/>
				<Item Name="Type Descriptor__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor__ogtk.ctl"/>
				<Item Name="Variant to Header Info__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Variant to Header Info__ogtk.vi"/>
				<Item Name="Waveform Subtype Enum__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Waveform Subtype Enum__ogtk.ctl"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Close.vi" Type="VI" URL="/&lt;vilib&gt;/MakerHub/PS4-Controller/Public/Close.vi"/>
				<Item Name="Conditional Auto-Indexing Tunnel (I32)--SUBROUTINE__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Conditional Auto-Indexing Tunnel (I32)--SUBROUTINE__JKI EasyXML.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Date Type Format String Mapping__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Date Type Format String Mapping__JKI EasyXML.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Easy Parse XML__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Easy Parse XML__JKI EasyXML.vi"/>
				<Item Name="EasyXML Options - Type Formatting Cluster__JKI EasyXML.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/EasyXML Options - Type Formatting Cluster__JKI EasyXML.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="Escape-Unescape String for XML__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Escape-Unescape String for XML__JKI EasyXML.vi"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="Formatting Data Type -- Enum__JKI EasyXML.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Formatting Data Type -- Enum__JKI EasyXML.ctl"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get Array Element Default Data--EasyXML__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Array Element Default Data--EasyXML__JKI EasyXML.vi"/>
				<Item Name="Get Attributes__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Attributes__JKI EasyXML.vi"/>
				<Item Name="Get Children__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Children__JKI EasyXML.vi"/>
				<Item Name="Get Default Data from TD--EasyXML__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Default Data from TD--EasyXML__JKI EasyXML.vi"/>
				<Item Name="Get Default Type Formatting__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Default Type Formatting__JKI EasyXML.vi"/>
				<Item Name="Get EasyXML Data Type__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get EasyXML Data Type__JKI EasyXML.vi"/>
				<Item Name="Get Element and Attributes from Tag__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Element and Attributes from Tag__JKI EasyXML.vi"/>
				<Item Name="Get Key-Value Pairs from Attribute List__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Key-Value Pairs from Attribute List__JKI EasyXML.vi"/>
				<Item Name="Get Next Name from Attribute List__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Next Name from Attribute List__JKI EasyXML.vi"/>
				<Item Name="Get Next Value from Attribute List__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Next Value from Attribute List__JKI EasyXML.vi"/>
				<Item Name="Get Node Attribute (String)__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Node Attribute (String)__JKI EasyXML.vi"/>
				<Item Name="Get Node Data (Boolean)__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Node Data (Boolean)__JKI EasyXML.vi"/>
				<Item Name="Get Node Data (DBL)__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Node Data (DBL)__JKI EasyXML.vi"/>
				<Item Name="Get Node Data (EXT)__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Node Data (EXT)__JKI EasyXML.vi"/>
				<Item Name="Get Node Data (I64)__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Node Data (I64)__JKI EasyXML.vi"/>
				<Item Name="Get Node Data (String)__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Node Data (String)__JKI EasyXML.vi"/>
				<Item Name="Get Node Data (U64)__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Node Data (U64)__JKI EasyXML.vi"/>
				<Item Name="Get Node Data__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Node Data__JKI EasyXML.vi"/>
				<Item Name="Get Root Elements__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Root Elements__JKI EasyXML.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Tag Content__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get Tag Content__JKI EasyXML.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Get UTC Offset__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Get UTC Offset__JKI EasyXML.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Index 1D Array Elements (I32)--SUBROUTINE__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Index 1D Array Elements (I32)--SUBROUTINE__JKI EasyXML.vi"/>
				<Item Name="Index 1D Array Elements (String)--SUBROUTINE__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Index 1D Array Elements (String)--SUBROUTINE__JKI EasyXML.vi"/>
				<Item Name="Is an Error (any error array element)__JKI Error Handling__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Is an Error (any error array element)__JKI Error Handling__JKI EasyXML.vi"/>
				<Item Name="Is an Error (error array)__JKI Error Handling__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Is an Error (error array)__JKI Error Handling__JKI EasyXML.vi"/>
				<Item Name="Is an Error (error cluster)__JKI Error Handling__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Is an Error (error cluster)__JKI Error Handling__JKI EasyXML.vi"/>
				<Item Name="Is an Error__JKI Error Handling__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Is an Error__JKI Error Handling__JKI EasyXML.vi"/>
				<Item Name="Link Children__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Link Children__JKI EasyXML.vi"/>
				<Item Name="Link XML Start Tags with End Tags__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Link XML Start Tags with End Tags__JKI EasyXML.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="Lookup Format String__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Lookup Format String__JKI EasyXML.vi"/>
				<Item Name="LV70DateRecToTimeStamp.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/LV70DateRecToTimeStamp.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="Multiline String to Array (Preserve EOLs)__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Multiline String to Array (Preserve EOLs)__JKI EasyXML.vi"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="NI_AALPro.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALPro.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_WebServices.lvlib" Type="Library" URL="/&lt;vilib&gt;/wsapi/NI_WebServices.lvlib"/>
				<Item Name="NI_XML.lvlib" Type="Library" URL="/&lt;vilib&gt;/xml/NI_XML.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Open First Controller.vi" Type="VI" URL="/&lt;vilib&gt;/MakerHub/PS4-Controller/Public/Open First Controller.vi"/>
				<Item Name="Open.vi" Type="VI" URL="/&lt;vilib&gt;/MakerHub/PS4-Controller/Public/Open.vi"/>
				<Item Name="Parse XML dateTime String__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Parse XML dateTime String__JKI EasyXML.vi"/>
				<Item Name="Parse XML for Tags__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Parse XML for Tags__JKI EasyXML.vi"/>
				<Item Name="Parse XML__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Parse XML__JKI EasyXML.vi"/>
				<Item Name="Read.vi" Type="VI" URL="/&lt;vilib&gt;/MakerHub/PS4-Controller/Public/Read.vi"/>
				<Item Name="Remove Comments from XML__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Remove Comments from XML__JKI EasyXML.vi"/>
				<Item Name="Remove DOCTYPE from XML__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Remove DOCTYPE from XML__JKI EasyXML.vi"/>
				<Item Name="Remove Headers from XML__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Remove Headers from XML__JKI EasyXML.vi"/>
				<Item Name="Remove Indentation__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Remove Indentation__JKI EasyXML.vi"/>
				<Item Name="Remove Raw XML Tag from Data Name__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Remove Raw XML Tag from Data Name__JKI EasyXML.vi"/>
				<Item Name="Search 1D Array (String)--SUBROUTINE__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Search 1D Array (String)--SUBROUTINE__JKI EasyXML.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Set VISA IO Session String__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Set VISA IO Session String__JKI EasyXML.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Toolkit Error Handling - Add Caller to API VI List__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Toolkit Error Handling - Add Caller to API VI List__JKI EasyXML.vi"/>
				<Item Name="Toolkit Error Handling - API VI List Buffer Core__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Toolkit Error Handling - API VI List Buffer Core__JKI EasyXML.vi"/>
				<Item Name="Toolkit Error Handling - Error Cluster From Error Code__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Toolkit Error Handling - Error Cluster From Error Code__JKI EasyXML.vi"/>
				<Item Name="Toolkit Error Handling - Get API VI List__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Toolkit Error Handling - Get API VI List__JKI EasyXML.vi"/>
				<Item Name="Toolkit Error Handling - Trim Call Chain at First API VI__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Toolkit Error Handling - Trim Call Chain at First API VI__JKI EasyXML.vi"/>
				<Item Name="Treat Data Name__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Treat Data Name__JKI EasyXML.vi"/>
				<Item Name="Treat Entity Name__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/Treat Entity Name__JKI EasyXML.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="XML Get Node Data by Variant__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/XML Get Node Data by Variant__JKI EasyXML.vi"/>
				<Item Name="XML Loop Stack Recursion__JKI EasyXML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/XML Loop Stack Recursion__JKI EasyXML.vi"/>
				<Item Name="XML Structure - Cluster__JKI EasyXML.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/XML Structure - Cluster__JKI EasyXML.ctl"/>
				<Item Name="XML Tag Type - Enum__JKI EasyXML.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/EasyXML/JKI_EasyXML.llb/XML Tag Type - Enum__JKI EasyXML.ctl"/>
				<Item Name="zeromq.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/zeromq/zeromq.lvlib"/>
			</Item>
			<Item Name="CurrentCode.ctl" Type="VI" URL="../../../URScript-for-LabVIEW/expressions/CurrentCode.ctl"/>
			<Item Name="CurrentPose.ctl" Type="VI" URL="../../../URScript-for-LabVIEW/utils/CurrentPose.ctl"/>
			<Item Name="DOMUserDefRef.dll" Type="Document" URL="DOMUserDefRef.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="StandOff.vi" Type="VI" URL="../HighLevelMovementManager/Movement/StandOff.vi"/>
			<Item Name="ws_runtime.dll" Type="Document" URL="ws_runtime.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="My Source Distribution" Type="Source Distribution">
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{43E54C80-20E2-4E0E-9E59-C95D5839B8D4}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">My Source Distribution</Property>
				<Property Name="Bld_excludedDirectory[0]" Type="Path">vi.lib</Property>
				<Property Name="Bld_excludedDirectory[0].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[1]" Type="Path">instr.lib</Property>
				<Property Name="Bld_excludedDirectory[1].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[2]" Type="Path">user.lib</Property>
				<Property Name="Bld_excludedDirectory[2].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[3]" Type="Path">resource/objmgr</Property>
				<Property Name="Bld_excludedDirectory[3].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[4]" Type="Path">/C/ProgramData/National Instruments/InstCache/17.0</Property>
				<Property Name="Bld_excludedDirectory[5]" Type="Path">/C/Users/guy_w/Documents/LabVIEW Data/2017(32-bit)/ExtraVILib</Property>
				<Property Name="Bld_excludedDirectoryCount" Type="Int">6</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/My Source Distribution</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{2DAEAD35-5942-42DD-817B-541B6CFB6C37}</Property>
				<Property Name="Bld_version.build" Type="Int">2</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Destination Directory</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/My Source Distribution</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/My Source Distribution/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{B97BF072-C8DD-4BAC-8AAD-C2413738EEB4}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[1].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/URScript.lvlib/SubVI's</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[1].type" Type="Str">Container</Property>
				<Property Name="Source[2].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[2].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/URScript.lvlib/Types</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].type" Type="Str">Container</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/URScript.lvlib/Global 1.vi</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].type" Type="Str">VI</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/HighLevelMovementManager/MovementManager/RotateJoints.vi</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Exclude</Property>
				<Property Name="Source[4].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">5</Property>
			</Item>
		</Item>
	</Item>
</Project>
