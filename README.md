## TODO

# API End Points

All Endpoints are appended to the route /hlmm/

## /home
### HTTP Method
Post
### Query Parameters
None
### Request Body
None

### Response Body
```
{
    Success:Boolean,
    Error: {
        status: Boolean,
        code: Integer,
        source: String
    }
}
```
## /lookAt
### HTTP Method
Post
### Query Parameters
enable="true/false"
### Request Body
```
{
    x: float,
    y: float,
    z: float
}
```

### Response Body
```
{
    Success:Boolean,
    Error: {
        status: Boolean,
        code: Integer,
        source: String
    }
}
```

## /moveJoints
### HTTP Method
Post
### Query Parameters
None
### Request Body
```
{
    Base: float,
    Shoulder: float,
    Elbow: float,
    "Wrist 1": float,
    "Wrist 2": float,
    "Wrist 3": float
}
```

## /status
### HTTP Method
Get
### Query Parameters
None
### Request Body
None

### Response Body
```
{
    PowerOn:Boolean,
    ProgramRunning: Boolean,
    PowerButtonPressed: Boolean,
    LookAtEnabled: Boolean,
    CurrentPose:{
        x: double,
        y: double,
        z: double,
        rx: double,
        ry: double,
        rz: double
    },
    JointPosition:{
        Base: double,
        Shoulder: double,
        Elbow: double,
        "Wrist 1": double,
        "Wrist 2": double,
        "Wrist 3": double
    }
}
```

## /translate
### HTTP Method
Post
### Query Parameters
None
### Request Body
```
{
    x: float,
    y: float,
    z: float
}
```

### Response Body
```
{
    Success:Boolean,
    Error: {
        status: Boolean,
        code: Integer,
        source: String
    }
}
```

## /toPos
### HTTP Method
Post
### Query Parameters
None
### Request Body
```
{
    x: float,
    y: float,
    z: float
}
```

### Response Body
```
{
    Success:Boolean,
    Error: {
        status: Boolean,
        code: Integer,
        source: String
    }
}
```

## /stop
### HTTP Method
Post
### Query Parameters
None
### Request Body
None

### Response Body
```
{
    Success:Boolean,
    Error: {
        status: Boolean,
        code: Integer,
        source: String
    }
}
```

## /setPseudoFreedrivePolygon
### HTTP Method
Post
### Query Parameters
None
### Request Body
```
{
    x: Array[double],
    y: Array[double],
    z: Array[double]
}
```

### Response Body
```
{
    Success:Boolean,
    Error: {
        status: Boolean,
        code: Integer,
        source: String
    }
}
```

## /runPseudoFreedriveMode
### HTTP Method
Post
### Query Parameters
None
### Request Body
None

### Response Body
```
{
    Success:Boolean,
    Error: {
        status: Boolean,
        code: Integer,
        source: String
    }
}
```

## /endPseudoFreedriveMode
### HTTP Method
Post
### Query Parameters
None
### Request Body
None

### Response Body
```
{
    Success:Boolean,
    Error: {
        status: Boolean,
        code: Integer,
        source: String
    }
}
```


# Motion
## Functions
- [x] conveyor_pulse_decode(type, A, B)
- [x] end_force_mode()
- [x] end_freedrive_mode()
- [x] end_teach_mode()
- [x] force_mode(task_frame, selection_vector, wrench, type, limits)
- [x] freedrive_mode()
- [x] get_conveyor_tick_count()
- [x] movec(pose_via, pose_to, a=1.2, v=0.25, r=0)
- [x] movej(q, a=1.4, v=1.05, t=0, r=0)
- [x] movel(pose, a=1.2, v=0.25, t=0, r=0)
- [x] movep(pose, a=1.2, v=0.25, r=0)
- [x] servoc(pose, a=1.2, v=0.25, r=0)
- [x] servoj(q, a, v, t=0.008, lookahead_time=0.1, gain=300)
- [x] set_conveyor_tick_count(tick_count, absolute_encoder_resolution=0)
- [x] set_pos(q)
- [x] speedj(qd, a, t_min)
- [x] speedl(xd, a, t_min)
- [x] stop_conveyor_tracking()
- [x] stopj(a)
- [x] stopl(a)
- [x] teach_mode()
- [x] track_conveyor_circular(center, ticks_per_revolution, rotate_tool)
- [x] track_conveyor_linear(direction, ticks_per_meter)
## Variables
- [x] 

# Internals
## Functions
- [x] force()
- [x] get_actual_joint_positions()
- [x] get_actual_joint_speeds()
- [x] get_actual_tcp_pose()
- [x] get_actual_tcp_speed()
- [x] get_controller_temp()
- [x] get_inverse_kin(x, qnear=[-1.6, -1.7, -2.2, -0.8, 1.6, 0.0]maxPositionError=0.0001, maxOrientationError=0.0001)
- [x] get_joint_temp(j)
- [x] get_joint_torques()
- [x] get_target_joint_positions()
- [x] get_target_joint_speeds()
- [x] get_target_tcp_pose()
- [x] get_target_tcp_speed()
- [x] get_tcp_force()
- [x] popup(s, title=’Popup’, warning=False, error=False)
- [x] powerdown()
- [x] set_gravity(d)
- [x] set_payload(m, CoG)
- [x] set_tcp(pose)
- [x] sleep(t)
- [x] sync()
- [x] textmsg(s1, s2=’’)


## Variables
- [x] 

# URMath
## Functions
- [x] acos(f)
- [x] asin(f)
- [x] atan(f)
- [x] atan2(x, y)
- [x] binary_list_to_integer(l)
- [x] ceil(f)
- [x] cos(f)
- [x] d2r(d)
- [x] floor(f)
- [x] get_list_length(v)
- [x] integer_to_binary_list(x)
- [x] interpolate_pose(p_from, p_to, alpha)
- [x] length(v)
- [x] log(b, f)
- [x] norm(a)
- [x] point_dist(p_from, p_to)
- [x] pose_add(p_1, p_2)
- [x] pose_dist(p_from, p_to)
- [x] pose_inv(p_from)
- [x] pose_sub(p_to, p_from)
- [x] pose_trans(p_from, p_from_to)
- [x] pow(base, exponent)
- [x] r2d(r)
- [x] random()
- [x] sin(f)
- [x] sqrt(f)
- [x] tan(f)

## Variables
- [x] 

# Interfaces
## Functions
- [x] get_analog_in(n)
- [x] get_analog_out(n)
- [x] get_configurable_digital_in(n)
- [x] get_configurable_digital_out(n)
- [x] get_digital_in(n)
- [x] get_digital_out(n)
- [x] get_euromap_input(port_number)
- [x] get_euromap_output(port_number)
- [x] get_flag(n)
- [x] get_standard_analog_in(n)
- [x] get_standard_analog_out(n)
- [x] get_standard_digital_in(n)
- [x] get_standard_digital_out(n)
- [x] get_tool_analog_in(n)
- [x] get_tool_digital_in(n)
- [x] get_tool_digital_out(n)
- [x] modbus_add_signal(IP, slave_number, signal_address, signal_type, signal_name)
- [x] modbus_delete_signal(signal_name)
- [x] modbus_get_signal_status(signal_name, is_secondary_program)
- [x] modbus_send_custom_command(IP, slave_number, function_code, data)
- [x] modbus_set_output_register(signal_name, register_value, is_secondary_program)
- [x] modbus_set_output_signal(signal_name, digital_value, is_secondary_program)
- [x] modbus_set_runstate_dependent_choice(signal_name, runstate_choice)
- [x] modbus_set_signal_update_frequency(signal_name, update_frequency)
- [x] read_port_bit(address)
- [x] read_port_register(address)
- [x] rpc_factory(type, url)
- [x] set_analog_inputrange(port, range)
- [x] set_analog_out(n, f)
- [x] set_analog_outputdomain(port, domain)
- [x] set_configurable_digital_out(n, b)
- [x] set_digital_out(n, b)
- [x] set_euromap_output(port_number, signal_value)
- [x] set_euromap_runstate_dependent_choice(port_number, runstate_choice)
- [x] set_flag(n, b)
- [x] set_standard_analog_input_domain(port, domain)
- [x] set_standard_analog_out(n, f)
- [x] set_standard_digital_out(n)
- [x] set_tool_analog_input_domain(port, domain)
- [x] set_tool_digital_out(n, b)
- [x] set_tool_voltage(voltage)
- [x] socket_close(socket_name=’socket_0’)
- [x] socket_get_var(name, socket_name=’socket_0’)
- [x] socket_open(address, port, socket_name=’socket_0’)
- [x] socket_read_ascii_float(number, socket_name=’socket_0’)
- [x] socket_read_binary_integer(number, socket_name=’socket_0’)
- [x] socket_read_byte_list(number, socket_name=’socket_0’)
- [x] socket_read_string(socket_name=’socket_0’, prefix=’’, suffix=’’)
- [x] socket_send_byte(value, socket_name=’socket_0’)
- [x] socket_send_int(value, socket_name=’socket_0’)
- [x] socket_send_line(str, socket_name=’socket_0’)
- [x] socket_send_string(str, socket_name=’socket_0’)
- [x] socket_set_var(name, value, socket_name=’socket_0’)
- [x] write_port_bit(address, value)
- [x] write_port_register(address, value)
## Variables
- [x] 