 function rotate(yaw, pitch, roll) {
	// Assuming the angles are in radians.
	const c1 = Math.cos(yaw/2);
	const s1 = Math.sin(yaw/2);
	const c2 = Math.cos(pitch/2);
	const s2 = Math.sin(pitch/2);
	const c3 = Math.cos(roll/2);
	const s3 = Math.sin(roll/2);
	const c1c2 = c1*c2;
	const s1s2 = s1*s2;
	let w =c1c2*c3 - s1s2*s3;
	let x =c1c2*s3 + s1s2*c3;
	let y =s1*c2*c3 + c1*s2*s3;
	let z =c1*s2*c3 - s1*c2*s3;
	let angle = 2 * Math.acos(w);
	let norm = x*x+y*y+z*z;
	if (norm < 0.001) { // when all euler angles are zero angle =0 so
		// we can set axis to anything to avoid divide by zero
		x=1;
		y=z=0;
	} else {
		norm = Math.sqrt(norm);
    	x /= norm;
    	y /= norm;
    	z /= norm;
    }
    return [x,y,z]
}
function calculateYawPitchRoll(currentPos,targetPos){
    const {x,y,z} = {x:targetPos.x-currentPos.x, y:targetPos.y-currentPos.y, z:targetPos.z-currentPos.z}
    console.log({x,y,z})
    let rotz = Math.atan2( y, x )
    let roty = Math.atan2( z * Math.cos(rotz), x )
    if (z >= 0) {
        roty = -Math.atan2( z * Math.cos(rotz), x );
     }else{
        roty = Math.atan2( z * Math.cos(rotz), -x );
     }
    let rotx = Math.atan2( Math.cos(rotz), Math.sin(rotz) * Math.sin(roty) )
    return {roll:rotx, yaw:roty, pitch:rotz}
}
const currentPos = {x:-474.46,y:-111.50,z:419.39}
const targetPos = {x:-484.46,y:-112.50,z:419.39}
const tpr = calculateYawPitchRoll(currentPos,targetPos)
console.log(tpr)
console.log(rotate(tpr.yaw,tpr.pitch,tpr.roll))